import Layout from 'components/Layout';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

// Hooks
import useGeneralState from 'hooks/useGeneralState';
import useMounted from 'hooks/useMounted';

import makeStyles from '@material-ui/core/styles/makeStyles';
import ImageLazy from 'components/ImageLazy';

const useStyles = makeStyles({
	pad: {
		padding: 10,
	},
});

function ProductoPage(props) {
	const classes = useStyles();
	const { idProducto } = props.match.params;
	const state = useGeneralState();

	useMounted(async () => {
		await state.getProductoAction(idProducto);
	});

	return (
		<Layout title={`Producto - ${state.producto?.nombre}`}>
			<Paper elevation={2} className={classes.pad}>
				<Grid container direction='row' justifyContent='space-evenly'>
					<Grid item xs>
						<ImageLazy
							width={500}
							src={state.producto?.imagenPrincipal}
							alt={state.producto?.nombre}
						/>
					</Grid>
					<Grid item xs>
						<Typography color='primary' variant='h4'>
							{state.producto?.nombre}
						</Typography>

						<List>
							<ListItem>
								<Typography
									color='textSecondary'
									variant='h6'
									style={{ marginRight: 10 }}
								>
									Precio
								</Typography>
								<ListItemText primary={`S/. ${state.producto?.precio}`} />
							</ListItem>
							<ListItem>
								<Typography
									color='textSecondary'
									variant='h6'
									style={{ marginRight: 10 }}
								>
									Marca
								</Typography>

								<ListItemText primary={state.producto?.marca?.nombre} />
								{state.producto?.marca?.imagen && (
									<ImageLazy
										width={50}
										src={state.producto?.marca?.imagen}
										alt={state.producto?.marca?.nombre}
									/>
								)}
							</ListItem>
							<ListItem>
								<Typography
									color='textSecondary'
									variant='h6'
									style={{ marginRight: 10 }}
								>
									Stock
								</Typography>

								<ListItemText primary={state.producto?.stock} />
							</ListItem>
						</List>
					</Grid>
				</Grid>
			</Paper>
		</Layout>
	);
}

export default ProductoPage;
